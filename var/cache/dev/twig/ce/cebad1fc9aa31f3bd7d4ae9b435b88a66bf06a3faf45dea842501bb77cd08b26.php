<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_658b6459f073e80fb626e3a581c9da610765d7553b46baa4fe841a8b84bd5b9d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'pageTitle' => [$this, 'block_pageTitle'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        $context["currentPath"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 1, $this->source); })()), "request", [], "any", false, false, false, 1), "attributes", [], "any", false, false, false, 1), "get", [0 => "_route"], "method", false, false, false, 1), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 1, $this->source); })()), "request", [], "any", false, false, false, 1), "attributes", [], "any", false, false, false, 1), "get", [0 => "_route_params"], "method", false, false, false, 1));
        // line 2
        echo "
<!doctype html>
<html lang='en'>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width,initial-scale=1.0'>
        <link href=\"https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css\" integrity=\"sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2\" rel=\"stylesheet\" crossorigin=\"anonymous\">
        <!--link href=\"https://unpkg.com/tabulator-tables@4.9.1/dist/css/tabulator.min.css\" rel=\"stylesheet\"-->
        <title>";
        // line 10
        $this->displayBlock('title', $context, $blocks);
        echo "IIP Gas Infrastructure Europe</title>
        ";
        // line 11
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 12
        echo "        ";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("app");
        echo "
    </head>
    <body>
        <main id=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["svelteId"]) || array_key_exists("svelteId", $context) ? $context["svelteId"] : (function () { throw new RuntimeError('Variable "svelteId" does not exist.', 15, $this->source); })()), "html", null, true);
        echo "\" data-url=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api");
        echo "\">
            <a href='";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
        echo "'><img src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img/logo.png"), "html", null, true);
        echo "\" alt=\"logo\" class=\"logo\"></a>
            <nav>
                <ul>
                    <li><a class=\"";
        // line 19
        if (twig_in_filter("news", (isset($context["currentPath"]) || array_key_exists("currentPath", $context) ? $context["currentPath"] : (function () { throw new RuntimeError('Variable "currentPath" does not exist.', 19, $this->source); })()))) {
            echo "selected";
        }
        echo "\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("news");
        echo "\">News</a></li>
                    ";
        // line 24
        echo "                </ul>
                <ul class=\"nav-right\">
                    ";
        // line 29
        echo "                    <li><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("feed_rss");
        echo "\" target=\"_blank\"><i class=\"fas fa-rss\"></i> RSS</a></li>
                    <li><a href=\"";
        // line 30
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("feed_atom");
        echo "\" target=\"_blank\"><i class=\"far fa-atom-alt\"></i> ATOM</a></li>
                    <li><a href=\"";
        // line 31
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("feed_json");
        echo "\" target=\"_blank\"><i class=\"fal fa-file-code\"></i> JSON</a></li>
                </ul>
            </nav>
            <div class=\"header\">
                ";
        // line 35
        if (((isset($context["svelteId"]) || array_key_exists("svelteId", $context) ? $context["svelteId"] : (function () { throw new RuntimeError('Variable "svelteId" does not exist.', 35, $this->source); })()) === "main")) {
            // line 36
            echo "                <span class=\"announcement\">
                    <strong><span id=\"loadedOn\"></span></strong>&nbsp;&nbsp;<i class=\"fad fa-history fa-2x fa-flip-horizontal\" title=\"Loaded on\"></i>
                </span>
                ";
        }
        // line 40
        echo "                <span class=\"title\">
                    ";
        // line 41
        $this->displayBlock('pageTitle', $context, $blocks);
        echo "REMIT Urgent Market Messages
                </span>
            </div>
            ";
        // line 56
        echo "            ";
        $this->displayBlock('body', $context, $blocks);
        // line 57
        echo "        </main>


        <script src=\"https://kit.fontawesome.com/3d888677a5.js\" crossorigin=\"anonymous\"></script>
        <script src=\"https://code.jquery.com/jquery-3.5.1.slim.min.js\" integrity=\"sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx\" crossorigin=\"anonymous\"></script>
        ";
        // line 63
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("app");
        echo "
        ";
        // line 64
        $this->displayBlock('javascripts', $context, $blocks);
        // line 65
        echo "
        <footer class=\"no-sticky\">
            <ul>
                <li class=\"right hide-xs\"><a href=\"http://jaic.be\" target=\"_blank\"><small>Powered by JAIC</small></a></li>
                <li class=\"hide-xs\">&copy; GIE AISBL 2020</li>
                ";
        // line 74
        echo "                <li><a href=\"https://www.gie.eu/index.php/contact-us\" target=\"_blank\">Contact</a></li>
                <li class=\"show-xs\">
                    <a href=\"http://jaic.be\" target=\"_blank\" style=\"float: right\"><small>Powered by JAIC</small></a>
                    &copy; GIE AISBL 2016
                </li>
            </ul>
        </footer>
    </body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 10
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 11
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 41
    public function block_pageTitle($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "pageTitle"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "pageTitle"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 56
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 64
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  247 => 64,  229 => 56,  211 => 41,  193 => 11,  175 => 10,  156 => 74,  149 => 65,  147 => 64,  143 => 63,  135 => 57,  132 => 56,  126 => 41,  123 => 40,  117 => 36,  115 => 35,  108 => 31,  104 => 30,  99 => 29,  95 => 24,  87 => 19,  79 => 16,  73 => 15,  66 => 12,  64 => 11,  60 => 10,  50 => 2,  48 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set currentPath = path(app.request.attributes.get('_route'), app.request.attributes.get('_route_params')) %}

<!doctype html>
<html lang='en'>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width,initial-scale=1.0'>
        <link href=\"https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css\" integrity=\"sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2\" rel=\"stylesheet\" crossorigin=\"anonymous\">
        <!--link href=\"https://unpkg.com/tabulator-tables@4.9.1/dist/css/tabulator.min.css\" rel=\"stylesheet\"-->
        <title>{% block title %}{% endblock %}IIP Gas Infrastructure Europe</title>
        {% block stylesheets %}{% endblock %}
        {{ encore_entry_link_tags('app') }}
    </head>
    <body>
        <main id=\"{{ svelteId }}\" data-url=\"{{ path('api') }}\">
            <a href='{{ path('home') }}'><img src=\"{{asset('build/img/logo.png')}}\" alt=\"logo\" class=\"logo\"></a>
            <nav>
                <ul>
                    <li><a class=\"{% if 'news' in currentPath %}selected{% endif %}\" href=\"{{ path('news') }}\">News</a></li>
                    {#
                    <li><a class=\"{% if 'faq' in currentPath %}selected{% endif %}\" href=\"{{ path('faq') }}\">FAQ</a></li>
                    <li><a class=\"{% if 'about-us' in currentPath %}selected{% endif %}\" href=\"{{ path('about-us') }}\">About us</a></li>
                    #}
                </ul>
                <ul class=\"nav-right\">
                    {#
                    <li><a class=\"{% if 'subscribe' in currentPath %}selected{% endif %}\" href=\"{{ path('subscribe') }}\"><i class=\"fas fa-envelope\"></i> Subscribe</a></li>
                    #}
                    <li><a href=\"{{ path('feed_rss') }}\" target=\"_blank\"><i class=\"fas fa-rss\"></i> RSS</a></li>
                    <li><a href=\"{{ path('feed_atom') }}\" target=\"_blank\"><i class=\"far fa-atom-alt\"></i> ATOM</a></li>
                    <li><a href=\"{{ path('feed_json') }}\" target=\"_blank\"><i class=\"fal fa-file-code\"></i> JSON</a></li>
                </ul>
            </nav>
            <div class=\"header\">
                {% if svelteId is same as('main') %}
                <span class=\"announcement\">
                    <strong><span id=\"loadedOn\"></span></strong>&nbsp;&nbsp;<i class=\"fad fa-history fa-2x fa-flip-horizontal\" title=\"Loaded on\"></i>
                </span>
                {% endif %}
                <span class=\"title\">
                    {% block pageTitle %}{% endblock %}REMIT Urgent Market Messages
                </span>
            </div>
            {#% if svelteId is same as('main') %}
            <div class=\"header\">
                <div class=\"btn-group\" role=\"group\" aria-label=\"Table filters\">
                    <button id=\"type-2\" type=\"button\" class=\"btn btn-secondary\">Gas</button>
                    <button id=\"type-3\" type=\"button\" class=\"btn btn-secondary\">Other Market Information</button>
                </div>
                <div class=\"btn-group\" role=\"group\" aria-label=\"Table filters\">
                    <button id=\"planned\" type=\"button\" class=\"btn btn-secondary\">Event: planned</button>
                    <button id=\"unplanned\" type=\"button\" class=\"btn btn-secondary\">Event: unplanned</button>
                </div>
            </div>
            {% endif %#}
            {% block body %}{% endblock %}
        </main>


        <script src=\"https://kit.fontawesome.com/3d888677a5.js\" crossorigin=\"anonymous\"></script>
        <script src=\"https://code.jquery.com/jquery-3.5.1.slim.min.js\" integrity=\"sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx\" crossorigin=\"anonymous\"></script>
        {{ encore_entry_script_tags('app') }}
        {% block javascripts %}{% endblock %}

        <footer class=\"no-sticky\">
            <ul>
                <li class=\"right hide-xs\"><a href=\"http://jaic.be\" target=\"_blank\"><small>Powered by JAIC</small></a></li>
                <li class=\"hide-xs\">&copy; GIE AISBL 2020</li>
                {#
                <li><a href=\"{{ path('disclaimer') }}\">Disclaimer</a></li>
                <li><a href=\"{{ path('privacy-policy') }}\">Privacy policy</a></li>
                #}
                <li><a href=\"https://www.gie.eu/index.php/contact-us\" target=\"_blank\">Contact</a></li>
                <li class=\"show-xs\">
                    <a href=\"http://jaic.be\" target=\"_blank\" style=\"float: right\"><small>Powered by JAIC</small></a>
                    &copy; GIE AISBL 2016
                </li>
            </ul>
        </footer>
    </body>
</html>
", "base.html.twig", "C:\\Users\\conta\\Projects\\iip.gie.eu\\templates\\base.html.twig");
    }
}
