<?php

namespace App\Feed;

use App\Service\REMITAPIService;
use Debril\RssAtomBundle\Exception\FeedException\FeedNotFoundException;
use Debril\RssAtomBundle\Provider\FeedProviderInterface;
use Exception;
use FeedIo\Feed;
use FeedIo\FeedInterface;
use FeedIo\Feed\Item;
use Generator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class UMMProvider implements FeedProviderInterface {
    private $REMITAPIService, $router;
    private $filter, $published, $start, $limit = 30, $days, $startDate, $endDate, $reportType, $status, $messageId, $messageType, $marketParticipant, $marketParticipantCode, $balancingZone, $balancingZoneCode, $asset, $assetCode, $unavailabilityType;
    const TYPE_1 = '', TYPE_2 = 'Gas', TYPE_3 = 'Other Market Information',
        REPORT_TYPES = [
        self::TYPE_1 => 'TYPE I',
        self::TYPE_2 => 'TYPE II - unavailability of gas facilities',
        self::TYPE_3 => 'TYPE III - other market information',
    ];
    const FIELDS = [
        'published' => null,
        'start' => 0,
        'limit' => 100,
        'days' => null,
        'startDate' => null,
        'endDate' => null,
        'reportType' => null,
        'status' => 0,
        'messageId' => 3,
        'messageType' => 3,
        'marketParticipant' => 1,
        'marketParticipantCode' => 1,
        'balancingZone' => 1,
        'balancingZoneCode' => 1,
        'asset' => 2,
        'assetCode' => 2,
        'unavailabilityType' => 0,
    ];

    public function __construct(REMITAPIService $REMITAPIService, UrlGeneratorInterface $router) {
        $this->REMITAPIService = $REMITAPIService;
        $this->router = $router;
        $this->filter = true;
    }

    /**
     * @param Request $request
     * @return Feed|FeedInterface
     * @throws Exception
     */
    public function getFeed(Request $request): FeedInterface {
        //set params
        self::setParams($request->query->all());

        $id = null;
        if($request->get('id')) {
            $id = $request->get('id');
        } elseif(strpos($request->getUri(), 'historical') !== false) {
            $id = 'historical';
        }
        if(!is_null($id)) {
            $this->startDate = null;
            $this->endDate = null;
            if(strtolower($id) === 'historical') {
                $this->filter = false; //do not filter on dates
                //$this->days = null;
            } elseif($id === '15') {
                $this->days = 14;
                $this->endDate = new \DateTime('today', new \DateTimeZone('UTC'));
            }
        }
        //check with params
        //dd($this->days, $this->startDate, $this->endDate);
        if (!is_null($this->days) && !is_null($this->startDate) && !is_null($this->endDate)) {
            //error > can't use days with both startDate and endDate
            throw new FeedNotFoundException();
        } else {
            try {
                //init set => no days, start or end dates set
                $minDate = new \DateTime('today', new \DateTimeZone('UTC'));
                $maxDate = new \DateTime('tomorrow', new \DateTimeZone('UTC'));
                if(!is_null($this->published)) {
                    $this->start = null;
                    //piped published tag: do a from/to thingie
                    $x = explode('|', $this->published);
                    if(count($x) < 2 && count($x) > 0) {
                        //no from/to, use now as to
                        $minDate = new \DateTime($x[0], new \DateTimeZone('UTC'));
                        $maxDate = new \DateTime('tomorrow', new \DateTimeZone('UTC'));
                    } elseif(count($x) >= 2) {
                        $minDate = new \DateTime($x[0], new \DateTimeZone('UTC'));
                        $maxDate = new \DateTime($x[1], new \DateTimeZone('UTC'));
                        $maxDate->add(new \DateInterval('P2D'));
                    }
                    //calculate days
                    $interval = date_diff($minDate, $maxDate);
                    // Display the result
                    $this->days = $interval->days;
                } elseif (!is_null($this->days)) {
                    $minDate->sub(new \DateInterval('P' . $this->days . 'D'));
                } else {
                    $this->published = new \DateTime('now', new \DateTimeZone('UTC'));
                    $this->published = $this->published->format('Y-m-d');
                    //$minDate->sub(new \DateInterval('PT3H'));
                }
                if (!is_null($this->startDate)) {
                    $minDate = new \DateTime($this->startDate, new \DateTimeZone('UTC'));
                    if (!is_null($this->days)) {
                        $interval = 'P' . $this->days . 'D';
                        $maxDate = clone $minDate;
                        $maxDate->add(new \DateInterval($interval));
                    } else {
                        $maxDate = new \DateTime('tomorrow', new \DateTimeZone('UTC'));
                    }
                }
                if (!is_null($this->endDate)) {
                    $maxDate = new \DateTime($this->endDate, new \DateTimeZone('UTC'));
                    if (!is_null($this->days)) {
                        $interval = 'P' . $this->days . 'D';
                        $minDate = clone $maxDate;
                        $minDate->sub(new \DateInterval($interval));
                    } else {
                        $minDate = new \DateTime('2010-01-01', new \DateTimeZone('UTC'));;
                    }
                }
                $this->startDate = $minDate;
                $this->endDate = $maxDate;
            } catch (Exception $exception) {
                //do nothing
                throw new FeedNotFoundException();
            }
        }
        //dd($this, $this->days, $this->startDate, $this->endDate);
        $route = $request->get('_route');
        $type = strtoupper(str_replace('feed_', '', $route));
        // build the feed the way you want
        $feed = new Feed();
        $feed->setLastModified(new \DateTime('now', new \DateTimeZone('UTC')));
        $feed->setTitle('GIE IIP '.$type.' Feed');
        $feed->setDescription($type.' feed for the Urgent Market Messages sent in on the GIE REMIT platform, consisting of Type 2 - Unavailabilities of Gas Facilities and Type 3 - Other Market Information UMMs. Default view is data submitted today.');
        $feed->setUrl($this->router->generate($route, [], UrlGeneratorInterface::ABSOLUTE_URL));
        $feed->setLanguage('en-us');
        $items = $this->getItems();
        foreach ($items as $item) {
            $feed->add($item);
        }
        //die();
        return $feed;
    }

    protected function getItems(): Generator {
        $storedItems = $this->fetchFromStorage();
        if (array_key_exists('error', $storedItems)) {
            //something went wrong: throw error page
            throw new FeedNotFoundException();
        }
        foreach ($storedItems as $storedItem) {
            if(is_object($storedItem)) {
                $storedItem = json_decode(json_encode($storedItem), true);
            }
            $ignore = false;

            $published = new \DateTime($storedItem['published']);
            /*if($this->filter) {
                if ($published < $this->startDate) continue;
                if ($published > $this->endDate) continue;
            }*/

            //if (is_array($reportType = $this->reportType)) {
            if (!is_null($reportType = $this->reportType)) {
                $types = [];
                //if(!empty(array_intersect($reportType, ['2', 'type-2', 'type 2', 'uogf', 'unavailabilityOfGasFacilities', 'unavailabilityOfGasFacilitiesReport']))) {
                if(in_array($reportType, ['2', 'type-2', 'type 2', 'uogf', 'unavailabilityOfGasFacilities', 'unavailabilityOfGasFacilitiesReport'])) {
                    $types[] = 'Gas';
                }
                //if(!empty(array_intersect($reportType, ['3', 'type-3', 'type 3', 'omi', 'otherMarketInformation', 'otherMarketInformationReport']))) {
                if(in_array($reportType, ['3', 'type-3', 'type 3', 'omi', 'otherMarketInformation', 'otherMarketInformationReport'])) {
                    $types[] = 'Other Market Information';
                }
                if(!in_array($storedItem['message']['reportType'], $types)) continue;
            }
            //dump($reportType);
            //die();
            //dump($storedItem);
            self::checkFilters($storedItem, $ignore);
            if($ignore) continue;

            //populate the item
            $item = new Item;
            $title = [
                strtoupper($storedItem['message']['unavailabilityType']),
                strtoupper(self::REPORT_TYPES[$storedItem['message']['reportType']]),
                strtoupper($storedItem['message']['messageType']),
                ($storedItem['asset'] && $storedItem['asset']['name']) ? $storedItem['asset']['name'] : null,
                implode(', ', array_column($storedItem['marketParticipant'], 'name')),
                ($storedItem['unavailable']) ? $storedItem['unavailable']['capacity'] . ' ' . $storedItem['unavailable']['unit'] : null,
            ];
            $item->setTitle(implode(' - ', array_filter($title)));
            $item->setLink($this->router->generate('details', ['messageId' => $storedItem['message']['messageId']], UrlGeneratorInterface::ABSOLUTE_URL));
            $item->setDescription('<![CDATA[ ' . base64_decode($storedItem['rss']) . ' ]]>');
            $item->setLastModified($published);
            $item->setPublicId($storedItem['message']['messageId']);
            yield $item;
        }
    }

    protected function fetchFromStorage(): array {
        try {
            //dd($this);
            if(!is_null($this->published)) {
                $params = 'published=' . $this->published . '&size=3000&page=1';
                $full = true;
            } else {
                if(empty($this->start)) {
                    $params = 'published=' . $this->startDate->format('Y-m-d') . '&size=3000&page=1';
                } else {
                    $params = 'from=' . $this->start . '&size=' . $this->limit;
                }
                $full = true;
            }
            $data = $this->REMITAPIService->getReports($params, $full);
            return $data['data'];
        } catch (ClientExceptionInterface $e) {
            return ['error' => ['type' => 'ClientException', 'code' => $e->getCode(), 'message' => $e->getMessage()]];
        } catch (DecodingExceptionInterface $e) {
            return ['error' => ['type' => 'DecodingException', 'code' => $e->getCode(), 'message' => $e->getMessage()]];
        } catch (RedirectionExceptionInterface $e) {
            return ['error' => ['type' => 'RedirectionException', 'code' => $e->getCode(), 'message' => $e->getMessage()]];
        } catch (ServerExceptionInterface $e) {
            return ['error' => ['type' => 'ServerException', 'code' => $e->getCode(), 'message' => $e->getMessage()]];
        } catch (TransportExceptionInterface $e) {
            return ['error' => ['type' => 'TransportException', 'code' => $e->getCode(), 'message' => $e->getMessage()]];
        }
    }

    protected function setParams($params) {
        foreach (self::FIELDS as $field => $ignore) {
            $returnField = (isset($params[$field]) && strlen($params[$field]) > 0) ?
                (in_array($field, ['messageType', 'marketParticipant', 'balancingZone', 'asset']) ? strtolower(str_replace('-', ' ', $params[$field])) : strtolower($params[$field]) ):
                null;
            $this->$field = $returnField;
            /*
            $this->$field = (in_array($field, ['marketParticipant', 'marketParticipantCode', 'balancingZone', 'balancingZoneCode', 'asset', 'assetCode'])) ?
                array_filter(explode(',', $returnField)):
                $returnField;
            if(is_array($this->$field) && count($this->$field) === 0) {
                $this->$field = null;
            }
            */
        }
        /*
        foreach ($fields as $field) {
            dump($field, $this->$field);
        }
        die('test');
        */
    }

    private function checkFilters($storedItem, &$ignore) {
        foreach(self::FIELDS as $field => $type) {
            if(is_null($type)) continue;
            if (!is_null($this->$field)) {
                switch ($type) {
                    case 0: // exact match
                        if (array_key_exists($field, $storedItem)) {
                            $storedItemField = $storedItem[$field];
                        } else if (array_key_exists($field, $storedItem['message'])) {
                            $storedItemField = $storedItem['message'][$field];
                        } else {
                            $storedItemField = null;
                            $ignore = true;
                        }
                        if (strtolower($storedItemField) !== $this->$field) $ignore = true;
                        break;
                    case 1: // code/name partial match in multidimensional array
                        $t = strpos(strtolower($field), 'code') ? 'code' : 'name';
                        $subField = str_ireplace($t, '', $field);
                        $mapped = array_map('strtolower', array_column($storedItem[$subField], $t));
                        $found = preg_grep('/.*' . $this->$field . '.*/', $mapped);
                        if (count($found) === 0) $ignore = true;
                        break;
                    case 2: // code/name partial match in field (singular array)
                        $t = strpos(strtolower($field), 'code') ? 'code' : 'name';
                        $subField = str_ireplace($t, '', $field);
                        $pos = strpos(strtolower($storedItem[$subField][$t]), $this->$field);
                        if ($pos === false) $ignore = true;
                        break;
                    case 3: // partial match
                        if (array_key_exists($field, $storedItem)) {
                            $storedItemField = $storedItem[$field];
                        } else if (array_key_exists($field, $storedItem['message'])) {
                            $storedItemField = $storedItem['message'][$field];
                        } else {
                            $storedItemField = null;
                            $ignore = true;
                        }
                        $pos = strpos(strtolower($storedItemField), $this->$field);
                        if ($pos === false) $ignore = true;
                        break;
                }
            }
        }
    }
    private function checkFiltersWithArrays($storedItem) {
        $fields = [
            //'reportType' => 0,
            'status' => 0,
            'messageId' => 3,
            'messageType' => 3,
            'marketParticipant' => 1,
            'marketParticipantCode' => 1,
            'balancingZone' => 1,
            'balancingZoneCode' => 1,
            'asset' => 2,
            'assetCode' => 2,
            'unavailabilityType' => 0,
        ];
        foreach($fields as $field => $type) {
            if ((is_array($this->$field) && count($this->$field) > 0) || !is_null($this->$field)) {
                //dump($field, $this->$field);
                switch ($type) {
                    case 0: // exact match
                        if (array_key_exists($field, $storedItem)) {
                            $storedItemField = $storedItem[$field];
                        } else if (array_key_exists($field, $storedItem['message'])) {
                            $storedItemField = $storedItem['message'][$field];
                        } else {
                            $storedItemField = null;
                            $ignore = true;
                        }
                        if(is_array($this->$field)) {
                            foreach($this->$field as $f) {
                                if (strtolower($storedItemField) === $f) $ignore = false;
                            }
                        } else {
                            if (strtolower($storedItemField) !== $this->$field) $ignore = true;
                        }
                        break;
                    case 1: // code/name partial match in multidimensional array
                        $t = strpos(strtolower($field), 'code') ? 'code' : 'name';
                        $subField = str_ireplace($t, '', $field);
                        $mapped = array_map('strtolower', array_column($storedItem[$subField], $t));
                        $found = [];
                        if(is_array($this->$field)) {
                            foreach($this->$field as $f) {
                                $found = array_merge($found, preg_grep('/.*' . $f . '.*/', $mapped));
                            }
                        } else {
                            $found = preg_grep('/.*' . $this->$field . '.*/', $mapped);
                        }
                        if (count($found) === 0) $ignore = true;
                        break;
                    case 2: // code/name partial match in field (singular array)
                        $t = strpos(strtolower($field), 'code') ? 'code' : 'name';
                        $subField = str_ireplace($t, '', $field);
                        if(is_array($this->$field)) {
                            foreach($this->$field as $f) {
                                $pos = strpos(strtolower($storedItem[$subField][$t]), $f);
                                if ($pos !== false) $ignore = false;
                            }
                        } else {
                            $pos = strpos(strtolower($storedItem[$subField][$t]), $this->$field);
                            if ($pos === false) $ignore = true;
                        }
                        break;
                    case 3: // partial match
                        if (array_key_exists($field, $storedItem)) {
                            $storedItemField = $storedItem[$field];
                        } else if (array_key_exists($field, $storedItem['message'])) {
                            $storedItemField = $storedItem['message'][$field];
                        } else {
                            $storedItemField = null;
                            $ignore = true;
                        }
                        if(is_array($this->$field)) {
                            foreach($this->$field as $f) {
                                $pos = strpos(strtolower($storedItemField), $f);
                                if ($pos !== false) $ignore = false;
                            }
                        } else {
                            $pos = strpos(strtolower($storedItemField), $this->$field);
                            if ($pos === false) $ignore = true;
                        }
                        break;
                }
            }
        }
    }
}