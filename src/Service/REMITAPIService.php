<?php

namespace App\Service;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class REMITAPIService {
    private $params, $url, $key, $client, $logger;

    /**
     * REMITAPIService constructor.
     * @param ParameterBagInterface $params
     * @param HttpClientInterface $client
     */
    public function __construct(ParameterBagInterface $params, HttpClientInterface $client, LoggerInterface $logger) {
        $this->url = $params->get('remit.url');
        $this->key = $params->get('remit.key');
        $this->client = $client;
        $this->params = $params;
		$this->logger = $logger;
    }

    /**
     * @param string $key
     * @param string|null $params
     * @param bool|null $full
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getReports(string $key, string $params = null, bool $full = null): array {
        $url = $this->url.'/umm';
        if($full) {
            $url = $url . '/full';
        }
        try {
            if (!is_null($params)) {
                $params_array = [];
                parse_str($params, $params_array);
                if (array_key_exists('filter', $params_array)) {
                    //search for filters
                    $filters = [];
                    $params = [];
                    foreach ($params_array as $param_key => $param_value) {
                        if (is_array($param_value) && $param_key === 'filter') {
                            $filters = $param_value;
                        } else {
                            $params[] = $param_key . '=' . $param_value;
                        }
                    }
                    $filters = array_shift($params_array);
                    foreach ($filters as $filter) {
                        $params[] = $filter['field'] . '=' . $filter['value'];
                    }
                    $url .= '?' . implode('&', $params);
                } else {
                    $url .= '?' . $params;
                }
            } else {
                $url .= '?page=1&size=3000';
            }
        } catch (\Exception $exception) {
            return [];
        }
        try {
            //dd($url);
            $response = $this->client->request(
                'GET',
                $url,
	            [
		            'headers' => [
			            'x-key' => $key,
		            ],
	            ]
            );
            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                return $response->toArray();
            } else {
                return [];
            }
        } catch (Exception $exception) {
            return [];
        }
    }

    /**
     * @param string $key
     * @param string $messageId
     *
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getReport(string $key, string $messageId): array {
        $url = $this->url.'/umm?messageId='.$messageId;
        try {
            $response = $this->client->request(
                'GET',
                $url,
	            [
		            'headers' => [
			            'x-key' => $key,
		            ],
	            ]
            );
            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                return $response->toArray();
            } else {
                return [];
            }
        } catch (Exception $exception) {
            var_dump($exception);
            return [];
        }
    }

    /**
     * @param string $key
     * @param string|null $params
     *
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getServiceAnnouncements(string $key, string $params = null): array {
        $url = $this->url.'/sas';
        if(!is_null($params)) {
            $url .= '?'.$params;
        }
        $response = $this->client->request(
            'GET',
            $url,
	        [
		        'headers' => [
			        'x-key' => $key,
		        ],
	        ]
        );
        $statusCode = $response->getStatusCode();
        if($statusCode === 200) {
            return $response->toArray();
        } else {
            return [];
        }
    }

    public function getRSS(): ?ResponseInterface {
        $url = $this->url.'/rss';
        try {
            return $this->client->request('GET', $url);
        } catch (TransportExceptionInterface $e) {
            return null;
        }
    }


    public function getCompanies($key, $params): array {
        $url = $this->url.'/companies/iip';
	    if(!is_null($params)) {
		    $url .= '?'.$params;
	    }
        try {
            $response = $this->client->request(
                'GET',
                $url,
                [
                    'headers' => [
                        'x-key' => $key,
                    ],
                ]
            );
            //dd($response->getContent());
            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                return $response->toArray();
            } else {
                return [];
            }
        } catch (Exception $exception) {
            var_dump($exception);
            return [];
        }
    }
	public function login($request): array {
		try {
			$content = (array)json_decode($request->getContent(), true);
			$url = $this->url . '/login';
			//$url = str_replace('remit', 'remittest', $url);
			$body = [
				'web' => 'iip',
				'email' => $content['login_email'],
				'password' => $content['login_password'],
			];
			$response = $this->client->request(
				'POST',
				$url,
				[
					'body' => $body,
					//'body' => $content,
				]
			);
			$statusCode = $response->getStatusCode();
			$content = json_decode($response->getContent());
			if ($statusCode === 200) {
				try {
					return (array)json_decode($response->getContent());
				} catch (\Exception $exception) {
					if ($content->email) {
						return ['error' => $content->email];
					} else if ($content->password) {
						return ['error' => $content->password];
					}
				}
			} else {
				return self::somethingWentWrong($statusCode);
			}
		} catch (Exception $exception) {
			return self::somethingWentWrong($exception->getMessage() . ' / ' . $exception->getFile() . ' - ' . $exception->getLine());
		} catch (TransportExceptionInterface | ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface$e) {
			return (array)$e;
		}
		return [];
	}
	
	public function forgot($request): array {
		try {
			$content = (array)json_decode($request->getContent(), true);
			$url = $this->url . '/forgot';
			//$url = str_replace('remit', 'remittest', $url);
			$response = $this->client->request(
				'POST',
				$url,
				[
					'body' => [
						'web' => 'iip',
						'email' => $content['login_email'],
					],
					//'body' => $content,
				]
			);
			$statusCode = $response->getStatusCode();
			$content = json_decode($response->getContent());
			if ($statusCode === 200) {
				try {
					return (array)json_decode($response->getContent());
				} catch (\Exception $exception) {
					if ($content->email) {
						return ['error' => $content->email];
					}
				}
			} else {
				return self::somethingWentWrong($statusCode);
			}
		} catch (Exception $exception) {
			return self::somethingWentWrong($exception->getMessage() . ' / ' . $exception->getFile() . ' - ' . $exception->getLine());
		} catch (TransportExceptionInterface | ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface$exception) {
			return (array)$exception;
		}
		return [];
	}
	
	public function register($request): array {
		try {
			$content = (array)json_decode($request->getContent(), true);
			$body = [
				'web' => 'iip',
			];
			foreach($content as $key => $value) {
				$body = array_merge($body, [str_replace('register_', '', $key) => $value]);
			}
			$body['slug'] = $body['first_name'].' '.$body['last_name'];
			//$url = str_replace(['remit', 'api'], ['remittest', 'auth'], $this->url) . '/register';
			$url = str_replace('api', 'auth', $this->url) . '/register';
			//$url = str_replace('remit', 'remittest', $url);
			$response = $this->client->request(
				'POST',
				$url,
				[
					'body' => $body,
				]
			);
			$statusCode = $response->getStatusCode();
			if ($statusCode === 200) {
				return (array)json_decode($response->getContent());
			} else {
				return self::somethingWentWrong($statusCode);
			}
		} catch (Exception $exception) {
			return self::somethingWentWrong($exception->getMessage() . ' / ' . $exception->getFile() . ' - ' . $exception->getLine());
		} catch (TransportExceptionInterface | ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface$e) {
			return (array)$e;
		}
	}
	
	public function update($request): array {
		try {
			$content = (array)json_decode($request->getContent(), true);
			$body = [
				'web' => 'iip',
			];
			foreach($content as $item) {
				$body = array_merge($body, [$item['name'] => $item['value']]);
			}
			$body['slug'] = $body['first_name'].' '.$body['last_name'];
			$url = str_replace('api', 'auth', $this->url) . '/update';
			//$url = str_replace('remit', 'remittest', $url);
			$response = $this->client->request(
				'POST',
				$url,
				[
					'body' => $body,
					//'body' => $content,
				]
			);
			$statusCode = $response->getStatusCode();
			if ($statusCode === 200) {
				return (array)json_decode($response->getContent());
			} else {
				return self::somethingWentWrong($url . ' - ' . $statusCode);
			}
		} catch (Exception $exception) {
			return self::somethingWentWrong($exception->getMessage() . ' / ' . $exception->getFile() . ' - ' . $exception->getLine());
		} catch (TransportExceptionInterface | ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface$e) {
			return (array)$e;
		}
	}
	
	public function delete($request): array {
		try {
			$content = (array)json_decode($request->getContent(), true);
			$body = [
				'web' => 'iip',
			];
			foreach($content as $item) {
				$body = array_merge($body, [$item['name'] => $item['value']]);
			}
			$body['slug'] = $body['first_name'].' '.$body['last_name'];
			//$url = str_replace(['remit', 'api'], ['remittest', 'auth'], $this->url) . '/delete';
			$url = str_replace('api', 'auth', $this->url) . '/delete';
			//$url = str_replace('remit', 'remittest', $url);
			$response = $this->client->request(
				'POST',
				$url,
				[
					'body' => $body,
					//'body' => $content,
				]
			);
			$statusCode = $response->getStatusCode();
			if ($statusCode === 200) {
				return (array)json_decode($response->getContent());
			} else {
				return self::somethingWentWrong($url . ' - ' . $statusCode);
			}
		} catch (Exception $exception) {
			return self::somethingWentWrong($exception->getMessage() . ' / ' . $exception->getFile() . ' - ' . $exception->getLine());
		} catch (TransportExceptionInterface | ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface$e) {
			return (array)$e;
		}
	}
	
	private function somethingWentWrong($extra = ''): array {
		return [
			'error' => '500',
			'message' => 'Something went wrong. If it continues, please contact api@gie.eu for assistance',
			'info' => $extra
		];
	}
}