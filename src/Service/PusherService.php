<?php
namespace App\Service;

use Pusher\Pusher;
use Pusher\PusherException;

class PusherService {
    private $pusher;

    /**
     * PusherService constructor.
     */
    public function __construct() {
        $options = array(
            'cluster' => 'eu',
            'useTLS' => true
        );
        try {
            $this->pusher = new Pusher(
                'd523c83e5c1bb5c4be5f',
                '1f3530dc03ba2291265c',
                '1108131',
                $options
            );
        } catch (PusherException $e) {
        }
    }

    /**
     * @param string[] $channels
     * @param string $event
     * @throws PusherException
     */
    public function call($channels = ['iip-channel'], $event = 'new-data-available'): void {
        $this->pusher->trigger($channels, $event, []);
    }
}