<?php

namespace App\Controller;

use App\Service\REMITAPIService;
use http\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class DefaultController extends AbstractController {
    private $REMITAPIService, $key;
	
	public function __construct(ParameterBagInterface $params, REMITAPIService $REMITAPIService) {
		$this->REMITAPIService = $REMITAPIService;
		$this->key = $params->get('remit.key');
	}

    /**
     * @Route("/", name="home")
     */
    public function index(): Response {
        return $this->render('index.html.twig', [
            'svelteId' => 'main',
            'params' => null,
        ]);
    }
	
	/**
	 * @Route("/details/{messageId}", name="details")
	 * @param Request $request
	 * @param string $messageId
	 *
	 * @return Response
	 */
    public function details(Request $request, string $messageId): Response {
	    $key = $this->getKey($request);
        $message = self::findUMM($key, $messageId);
        if(array_key_exists('data', $message) && count($message['data']) > 0) {
            $message = $message['data'][0];
            $total = (array_key_exists('prev', $message)) ? count($message['prev']) + 1 : 1;
            return $this->render('detail.html.twig', [
                'svelteId' => 'detail',
                'params' => null,
                'messageId' => $messageId,
                'message' => $message,
                'total' => $total,
            ]);
        } else {
            return $this->render('404.html.twig', [
                'svelteId' => '404',
                'params' => null,
                'page' => 'UMM report with ID '.$messageId,
            ]);

        }
    }
	
	/**
	 * @Route("/download/xml/{messageId}", name="download_xml")
	 * @param Request $request
	 * @param string $messageId
	 *
	 * @return Response
	 */
    public function download_xml(Request $request, string $messageId): Response {
	    $key = $this->getKey($request);
        $message = self::findUMM($key, $messageId);
        if (array_key_exists('data', $message) && count($message['data']) > 0) {
            $message = $message['data'][0];
            header('Content-type: text/xml');
            header('Content-Disposition: attachment; filename="' . $message['message']['messageId'] . '.xml"');
            echo base64_decode($message['rss']);
            die();
        } else {
            return $this->render('404.html.twig', [
                'svelteId' => '404',
                'params' => null,
                'page' => 'UMM report with ID ' . $messageId,
            ]);
        }
    }
	
	private function getKey($request) {
		$headers = $request->headers;
		if(
			!APIController::isCurlRequest() ||
			in_array($headers->get('sec-fetch-mode'), ['navigate', 'cors'])
			||
			strpos(strtolower($headers->get('user-agent')), 'mac') !== false
		) {
			$key = $this->key;
		} else {
			$key = $headers->get('x-key');
		}
		return $key;
	}

    private function findUMM($key, $messageId): array {
        try {
            $return = $this->REMITAPIService->getReport($key, $messageId);
        } catch (ClientExceptionInterface $e) {
            $return = ['type' => 'ClientException', 'code' => $e->getCode(), 'message' => $e->getMessage()];
        } catch (DecodingExceptionInterface $e) {
            $return = ['type' => 'DecodingException', 'code' => $e->getCode(), 'message' => $e->getMessage()];
        } catch (RedirectionExceptionInterface $e) {
            $return = ['type' => 'RedirectionException', 'code' => $e->getCode(), 'message' => $e->getMessage()];
        } catch (ServerExceptionInterface $e) {
            $return = ['type' => 'ServerException', 'code' => $e->getCode(), 'message' => $e->getMessage()];
        } catch (TransportExceptionInterface $e) {
            $return = ['type' => 'TransportException', 'code' => $e->getCode(), 'message' => $e->getMessage()];
        }
        return $return;
    }

    private function search($return, $messageId): array {
        $returnReport = null;
        $prev = [];
        $next = [];
        $searchForMessageId = explode('_', $messageId)[0];
        $array = array_filter($return, function($el) use ($searchForMessageId) {
            return ( strpos($el['message']['messageId'], $searchForMessageId) !== false );
        });
        if(count($array) > 1) {
            $array = array_shift($array);
            $previous = (isset($array['prev'])) ?$array['prev'] : [];
            unset($array['prev']);
            $searchArray = array_merge([$array], $previous);
            $total = count($searchArray);
            $stop = false;
            foreach($searchArray as $innerKey => $item) {
                if(is_array($item) && array_key_exists('message', $item) && strpos($item['message']['messageId'], $messageId) !== false) {
                    //found exact match
                    $returnReport = $item;
                    $stop = true;
                    continue;
                } elseif($stop) {
                    $prev[] = $item;
                } else {
                    $next[] = $item;
                }
            }
            $returnReport['prev'] = array_reverse($prev);
            $returnReport['next'] = array_reverse($next);
            $returnReport['total'] = $total;
        }
        if(is_null($returnReport)) {
            throw $this->createNotFoundException('The UMM with messageId ' . $messageId . ' does not exist');
        }
        return $returnReport;
    }

    /**
     * @Route("/news/{messageId?}", name="news")
     */
    public function news(string $messageId = null): Response {
	    $param = (!is_null($messageId)) ?
			'?url='.$messageId :
	        '';
        return $this->render('news.html.twig', [
            'svelteId' => 'news',
            'params' => $param,
        ]);
    }

    /**
     * @Route("/data-definition", name="data-definition")
     */
    public function dataDefinition(): Response {
        return $this->render('data-definition.html.twig', [
            'svelteId' => 'data-definition',
            'params' => null,
        ]);
    }

    /**
     * @Route("/transparency", name="transparency")
     */
    public function transparency(): Response {
        return $this->render('transparency.html.twig', [
            'svelteId' => 'about',
            'params' => null,
        ]);
    }

    /**
     * @Route("/disclaimer", name="disclaimer")
     */
    public function disclaimer(): Response {
        return $this->render('disclaimer.html.twig', [
            'svelteId' => 'disclaimer',
	        'params' => null,
        ]);
    }

    /**
     * @Route("/privacy-policy", name="privacy-policy")
     */
    public function privacyPolicy(): Response {
        return $this->render('privacy.html.twig', [
            'svelteId' => 'privacy-policy',
	        'params' => null,
        ]);
    }

    /**
     * @Route("/subscribe", name="subscribe")
     */
    public function subscribe(): Response {
        return $this->render('subscribe.html.twig', [
            'svelteId' => 'subscribe',
	        'params' => null,
        ]);
    }

    /**
     * @Route("/account", name="account")
     */
    public function account(Request $request): Response {
	    $session = $request->getSession();
	    // stores an attribute in the session for later reuse
	    //$session->set('is_logged_in', []);
	    // gets an attribute by name
	    $logged_in = $session->get('is_logged_in');
	    if($logged_in) {
		    return $this->render('account.html.twig', [
			    'svelteId' => 'account',
			    'params' => json_encode($logged_in),
		    ]);
	    } else {
		    return $this->render('login.html.twig', [
			    'svelteId' => 'login',
			    'params' => null,
		    ]);
	    }
    }
}
