<?php

namespace App\Controller;

use App\Service\REMITAPIService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class APIController extends AbstractController {
    private $REMITAPIService, $key;

    public function __construct(ParameterBagInterface $params, REMITAPIService $REMITAPIService) {
        $this->REMITAPIService = $REMITAPIService;
	    $this->key = $params->get('remit.key');
    }
	
    /**
     * @Route("/api/login", name="post_login", methods={"POST"})
     * @param Request $request
     * @return array|JsonResponse
     */
    public function login(Request $request) {
        $session = $request->getSession();
        $return = $this->REMITAPIService->login($request);
        if(array_key_exists('access_token', $return)) {
            // stores an attribute in the session for later reuse
            $session->set('is_logged_in', $return);
            return $this->json(['success' => 'logged in']);
        } else {
            return $this->json($return);
        }
    }

    /**
     * @Route("/api/forgot", name="post_forgot", methods={"POST"})
     * @param Request $request
     * @return array|JsonResponse
     */
    public function forgot(Request $request) {
        $return = $this->REMITAPIService->forgot($request);
        return (is_array($return)) ?
            $this->json($return) :
            $return;
    }

    /**
     * @Route("/api/register", name="post_register", methods={"POST"})
     * @param Request $request
     * @return array|JsonResponse
     */
    public function register(Request $request) {
        $session = $request->getSession();
        $return = $this->REMITAPIService->register($request);
        if(array_key_exists('access_token', $return)) {
            // stores an attribute in the session for later reuse
            $session->set('is_logged_in', $return);
            return $this->json(['success' => 'logged in']);
        } else {
            return $this->json($return);
        }
    }

    /**
     * @Route("/api/update", name="post_update", methods={"POST"})
     * @param Request $request
     * @return array|JsonResponse
     */
    public function update(Request $request) {
        $session = $request->getSession();
        if($session->get('is_logged_in')) {
            $return = $this->REMITAPIService->update($request);
            if (array_key_exists('api_access', $return)) {
                // stores an attribute in the session for later reuse
                $return = (array_replace($session->get('is_logged_in'), $return));
                $session->set('is_logged_in', $return);
                return $this->json(['success' => 'updated']);
            } else {
                return $this->json($return);
            }
        } else {
            return $this->json(['error' => 'something went wrong']);
        }
    }

    /**
     * @Route("/api/delete", name="post_delete", methods={"POST"})
     * @param Request $request
     * @return array|JsonResponse
     */
    public function delete(Request $request) {
        $session = $request->getSession();
        $return = $this->REMITAPIService->delete($request);
        if(array_key_exists('deleted', $return)) {
            $session->clear();
            return $this->json(['success' => 'account removed']);
        } else {
            return $this->json($return);
        }
    }

    /**
     * @Route("/api/logout", name="post_logout", methods={"POST"})
     * @param Request $request
     * @return array|JsonResponse
     */
    public function logout(Request $request) {
        $session = $request->getSession();
        $session->clear();
        return $this->json(['success' => 'logged out']);
    }

    /**
     * @Route("/api/letusclearthecache", name="api_clearcaches")
     */
    public function clearCaches() {
        $cache = new FilesystemAdapter();
        $cache->clear();
        return $this->json(['cache cleared']);
    }
	
	public static function isCurlRequest(): bool {
		// Check for the presence of the HTTP_USER_AGENT header
		if (isset($_SERVER['HTTP_USER_AGENT'])) {
			// Look for common cURL user agents
			if (strpos($_SERVER['HTTP_USER_AGENT'], 'curl') !== false) {
				return true;
			}
		}
		
		// Check for the presence of the HTTP_X_REQUESTED_WITH header
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
			// cURL requests often set this header
			if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
				return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * @Route("/api/{action}", name="api")
	 * @param Request $request
	 * @param string $action
	 * @return Response
	 */
	public function api(Request $request, string $action = 'reports'): Response {
		//$params = [];
		//parse_str($request->getQueryString(), $params);
		$params = $request->getQueryString();
		$headers = $request->headers;
		if(
			!self::isCurlRequest() ||
			in_array($headers->get('sec-fetch-mode'), ['navigate', 'cors'])
			||
			strpos(strtolower($headers->get('user-agent')), 'mac') !== false
		) {
			$key = $this->key;
		} else {
			$key = $headers->get('x-key');
		}
		switch ($action) {
			case 'news':
				$function = 'getServiceAnnouncements';
				break;
			case 'about':
				$function = 'getCompanies';
				break;
			case 'reports':
			default:
				$function = 'getReports';
		}
		try {
			$return = $this->REMITAPIService->$function($key, $params);
		} catch (ClientExceptionInterface $e) {
			$return = ['type' => 'ClientException', 'code' => $e->getCode(), 'message' => $e->getMessage()];
		} catch (DecodingExceptionInterface $e) {
			$return = ['type' => 'DecodingException', 'code' => $e->getCode(), 'message' => $e->getMessage()];
		} catch (RedirectionExceptionInterface $e) {
			$return = ['type' => 'RedirectionException', 'code' => $e->getCode(), 'message' => $e->getMessage()];
		} catch (ServerExceptionInterface $e) {
			$return = ['type' => 'ServerException', 'code' => $e->getCode(), 'message' => $e->getMessage()];
		} catch (TransportExceptionInterface $e) {
			$return = ['type' => 'TransportException', 'code' => $e->getCode(), 'message' => $e->getMessage()];
		}
		
		return $this->json($return);
	}
}