/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './js/slick/slick.css';
import './js/slick/slick-theme.css';
import './styles/app.css';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';
import './js/slick/slick';

import App from './js/App.svelte';
import News from './js/News.svelte';
import About from './js/About.svelte';
import Account from './js/Account.svelte';
import Login from "./js/Login.svelte";


if(document.getElementById('main') !== null) {
    const app = new App({
        target: document.getElementById('main')//document.body,
    });
}
if(document.getElementById('news') !== null) {
    const news = new News({
        target: document.getElementById('news')//document.body,
    });
}
if(document.getElementById('about') !== null) {
    const about = new About({
        target: document.getElementById('about')//document.body,
    });
}
if(document.getElementById('account') !== null) {
    const account = new Account({
        target: document.getElementById('account')//document.body,
    });
}
if(document.getElementById('login') !== null) {
    const login = new Login({
        target: document.getElementById('login')//document.body,
    });
}



$('.carousel').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    draggable: false,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 6000,
    adaptiveHeight: true,
    adaptiveWidth: true,
});

$(document).on('click', '.title-tooltip', function (el) {
    let tooltip = '#tooltip-screen',
        text = $(this).attr('title'),
        width = $(this).parents('.tabulator-col:first').width() - 25;
    $(tooltip).css({top: el.pageY + 15, left: el.pageX - width, position: 'absolute'}).html(text).toggle();
});
$(document).on('mouseleave', '.tabulator-col', function (el) {
    let tooltip = '#tooltip-screen';
    $(tooltip).html('').hide();
});